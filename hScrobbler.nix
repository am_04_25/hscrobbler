{ mkDerivation, aeson, base, hashmap, liblastfm
, optparse-applicative, process, stdenv, taglib, text, time
, unordered-containers
}:
mkDerivation {
  pname = "hScrobbler";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base hashmap liblastfm optparse-applicative process taglib
    text time unordered-containers
  ];
  homepage = "https://bitbucket.org/am_04_25/hscrobbler";
  description = "Last.fm scrobbler for mplayer";
  license = stdenv.lib.licenses.publicDomain;
}
