{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc865"}:
(import ./default.nix { inherit nixpkgs compiler; })
  .env.overrideAttrs (oldAttrs: with nixpkgs; {
    buildInputs = oldAttrs.buildInputs ++ [ zsh vim cabal-install ];
    shellHook = oldAttrs.shellHook + ''
      zsh -c "PATH=$PATH; zsh"
      exit
    '';
  })
