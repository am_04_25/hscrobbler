{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc844" }:

let
  config = {
    packageOverrides = oldpkgs: let newpkgs = oldpkgs.pkgs; in {
      haskellPackages = oldpkgs.haskellPackages.override {
        overrides = self: super: {
          # because cereal-0.5.7.0 fails
          cereal        = self.callPackage ./nix/cereal-0.5.8.0.nix {};
          contravariant = self.callPackage ./nix/contravariant-1.5.nix {};
          aeson         = oldpkgs.haskell.lib.dontCheck (self.callPackage ./nix/aeson-1.4.2.0.nix {});

          xml-html-conduit-lens = self.callPackage ../xml-html-conduit-lens/package.nix {};

          liblastfm = self.callPackage ./nix/liblastfm-0.7.0.nix {};
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };
in
  pkgs.haskellPackages.callPackage ./hScrobbler.nix { liblastfm = pkgs.haskellPackages.liblastfm; aeson = pkgs.haskellPackages.aeson; }
