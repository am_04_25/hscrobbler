{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent
import Control.Exception (try)
import Control.Monad (sequence,when)
import Data.Aeson.Types (Value(..))
import Data.HashMap.Lazy (toList)
import Data.List.NonEmpty (NonEmpty (..))
import Data.Semigroup ((<>))
import Data.Time.Clock.POSIX (getPOSIXTime)
import Lastfm
import Lastfm.Authentication
import Lastfm.Track (item,scrobble)
import System.Environment (setEnv,getEnv,unsetEnv)
import System.Process
import qualified Data.Text           as S
import qualified Data.Text.Lazy      as L
import qualified Data.Text.Lazy.IO   as L
import qualified GHC.IO.Handle       as H
import qualified Options.Applicative as O
import qualified Sound.TagLib        as Tag
import qualified System.IO           as IO



data ArtistTrack = AT
  { thisArtist :: S.Text
  , thisTrack  :: S.Text
  }

instance Show ArtistTrack where
  show (AT a t) = S.unpack a ++ " --- " ++ S.unpack t

data Manual = Manual
  { artr  :: ArtistTrack
  , quiet :: Bool
  } deriving Show

data Mplayer = Mplayer
  { mplayerOptions :: String
  , percentage     :: Float
  } deriving Show

data HowToScrobble
  = Man Manual
  | Mpl Mplayer
  | Auth
  deriving Show



main :: IO ()
main = O.execParser hScrobblerInfo >>=
  (\case
    Man man -> manScrobbler man
    Mpl mpl -> mplScrobbler mpl
    Auth    -> auth
  )



artistOpt :: O.Parser S.Text
artistOpt = S.pack <$> O.strOption
  (  O.long    "artist"
  <> O.short   'a'
  <> O.metavar "STRING"
  <> O.help    "Artist name of the track to scrobble")

trackOpt :: O.Parser S.Text
trackOpt = S.pack <$> O.strOption
  (  O.long    "track"
  <> O.short   't'
  <> O.metavar "STRING"
  <> O.help    "Title of the track to scrobble")

quietOpt :: O.Parser Bool
quietOpt = O.switch
  (  O.long  "quiet"
  <> O.short 'q'
  <> O.help  "Whether to be quiet")

mplayerOpt :: O.Parser String
mplayerOpt = O.strOption
  (  O.long    "mplayer"
  <> O.short   'm'
  <> O.metavar "STRING"
  <> O.help    "Arguments of mplayer")

percentOpt :: O.Parser Float
percentOpt = let def = 50 in
  readOrDefault def <$> O.strOption
    (  O.long    "at"
    <> O.value   (show def)
    <> O.metavar "FLOAT"
    <> O.help    "Scrobbling point of the track (%) (default: 50)")

artrOpt :: O.Parser ArtistTrack
artrOpt = AT <$> artistOpt <*> trackOpt

manual :: O.Parser HowToScrobble
manual = Man <$> (Manual <$> artrOpt <*> quietOpt)

mplayer :: O.Parser HowToScrobble
mplayer = Mpl <$> (Mplayer <$> mplayerOpt <*> percentOpt)

authOpt :: O.Parser HowToScrobble
authOpt = O.flag' Auth
  (  O.long "auth"
  <> O.help "Do authentication")

howToScrobble :: O.Parser HowToScrobble
howToScrobble = manual <|> mplayer <|> authOpt

hScrobblerInfo :: O.ParserInfo HowToScrobble
hScrobblerInfo = O.info (howToScrobble <**> O.helper)
  (  O.fullDesc
  <> O.header "hScrobbler - Last.fm scrobbler for mplayer")



readOrDefault :: Read a => a -> String -> a
readOrDefault def s = case reads s of
  ((x,_):_) -> x
  _         -> def



apiKeyEnv     = "HSCROBBLER_APIKEY"
secretEnv     = "HSCROBBLER_SECRET"
sessionKeyEnv = "HSCROBBLER_SESSIONKEY"



with2Envs :: (S.Text -> S.Text -> IO ()) -> IO ()
with2Envs f = (try $ S.pack <$> getEnv apiKeyEnv) >>= either
  ((\e -> putStrLn $
          "Set the correct \"API Key\" registered at "
       ++ "\"https://www.last.fm/api/account/create\" to \"$"
       ++ apiKeyEnv ++ "\"") :: IOError -> IO ())
  (\thisAPIKey -> (try $ S.pack <$> getEnv secretEnv) >>= either
    ((\e -> putStrLn $
            "Set the correct \"Shared secret\" registered at "
         ++ "\"https://www.last.fm/api/account/create\" to \"$"
         ++ secretEnv ++ "\"") :: IOError -> IO ())
    (\thisSecret -> f thisAPIKey thisSecret))



with3Envs :: (S.Text -> S.Text -> S.Text -> IO ()) -> IO ()
with3Envs f = with2Envs $ \thisAPIKey thisSecret ->
  (try $ S.pack <$> getEnv sessionKeyEnv) >>= either
    ((\e -> putStrLn $
            "Execute \"hScrobbler --auth\" "
         ++ "and set correct value to \"$"
         ++ sessionKeyEnv ++ "\"") :: IOError -> IO ())
    (\thisSessionKey -> f thisAPIKey thisSecret thisSessionKey)



manScrobbler :: Manual -> IO ()
manScrobbler man =
  with3Envs $ \thisAPIKey thisSecret thisSessionKey -> do
    con <- newConnection
    now <- fromIntegral <$> round <$> getPOSIXTime
    let scr   = return $ item
                <*> artist (thisArtist $ artr $ man)
                <*> track  (thisTrack  $ artr $ man)
                <*> timestamp now
    let ready = sign (Secret thisSecret)
                $ scrobble scr
                <*> apiKey thisAPIKey
                <*> sessionKey thisSessionKey
                <*  json
    lastfm con ready >>= either
      (\e -> print e)
      (\_ -> if quiet man then return ()
             else putStrLn
              $ "Scrobbled: \"" ++ (show $ artr man) ++ "\"")



getArTrFromTag :: String -> IO (Maybe ArtistTrack)
getArTrFromTag path = Tag.open path >>= maybe
  (putStrLn ("Failed to read ID3 tag from " ++ path)
    >> return Nothing)
  (\ftag -> Tag.tag ftag >>= maybe
    (putStrLn ("Failed to get artist name and title of " ++ path)
      >> return Nothing)
    (\tag -> do
      thisArtist <- Tag.artist tag
      thisTitle  <- Tag.title  tag
      return $ Just (AT (S.pack thisArtist) (S.pack thisTitle))
      )
    )



mplScrobbler :: Mplayer -> IO ()
mplScrobbler mpl = withCreateProcess
  (shell $ "mplayer -novideo " ++ mplayerOptions mpl)
  { delegate_ctlc = True
  , std_out = CreatePipe
  , std_err = NoStream
  }
  $ \_ stdout _ ph -> do
    outSId <- forkIO $ flip (maybe (return ())) stdout $
      (\h -> do
        H.hSetBuffering IO.stdout H.NoBuffering
        byNESC mpl h
        )
    waitForProcess ph
    killThread outSId
    putChar '\n'



byNESC :: Mplayer -> H.Handle -> IO ()
byNESC mpl handle = do
  mATMV <- newMVar Nothing
  contents <- L.hGetContents handle
  let lines = L.split (\c -> '\n' == c || '\ESC' == c)
                      contents
  sequence $ flip map lines $ \l -> maybe
    (when (L.isPrefixOf "[J\rA: " l) $ do
    L.putStr $ L.cons '\ESC' l
    let pInfo = filter (not . (== ""))
                $ L.split (== ' ') l
    let now = (read $ L.unpack $ pInfo !! 1 :: Float)
    let len = (read $ L.unpack $ pInfo !! 4 :: Float)
    let percent = now / len * 100
    when (percent >= percentage mpl) $ do
      L.putStr " [S]"
      takeMVar mATMV >>= maybe (putMVar mATMV Nothing)
        (\at -> do
          putMVar mATMV Nothing
          manScrobbler $ Manual at True
        )
    )
    (\pdot -> do
      putChar '\n'
      mAT <- getArTrFromTag $ L.unpack $ L.init pdot
      swapMVar mATMV mAT
      maybe (return ()) (\at -> do
        putStrLn $ "Playing: " ++ (S.unpack $ thisArtist at)
                ++ " --- "     ++ (S.unpack $ thisTrack  at)
        ) mAT
    )
    (L.stripPrefix "Playing " l)
  return ()



auth :: IO ()
auth = with2Envs $ \thisAPIKey thisSecret -> do
  con <- newConnection
  (lastfm con $ getToken <*> apiKey thisAPIKey <* json) >>= either
    (\e -> (putStrLn $ "-- Check that the value of \"$"
                    ++ apiKeyEnv ++ "\" is correct"
                    ++ "\n-- Current Value: "
                    ++ S.unpack thisAPIKey
                    ++ "\n-- Error ↓")
                >> print e)

    (\(Object tokenObj) -> do
    let tokenText = (\(String x) -> x)
          $ snd $ head $ toList $ tokenObj
    putStrLn "Access the URL below and complete the authentication"
    putStrLn . link $ apiKey thisAPIKey <* token tokenText
    putStrLn "\n-- Press Enter when done --"
    getLine
    (lastfm con $ sign (Secret thisSecret) $ getSession
          <*> token  tokenText
          <*> apiKey thisAPIKey
          <*  json) >>= either
      (\e -> (putStrLn $ "-- Check that the value of \"$"
                      ++ secretEnv ++ "\" is correct"
                      ++ "\n-- Current Value: "
                      ++ S.unpack thisSecret
                      ++ "\n-- Error ↓")
                  >> print e)

      (\(Object authObj) -> do
      let thisSessionKey = S.unpack $
            (\(Object h) -> (\(String s) -> s) $ snd $ toList h !! 1)
            $ snd $ head $ toList $ authObj
      setEnv sessionKeyEnv thisSessionKey
      putStrLn $   "Do "
                ++ "\"export " ++ sessionKeyEnv ++ "="
                ++ thisSessionKey ++ "\""
        ++ "\n" ++ "every time before scrobbling"
      ))
