# hScrobbler - Last.fm scrobbler for mplayer
https://www.last.fm  
http://www.mplayerhq.hu  

## Usage:  
hScrobbler ((-a|--artist STRING) (-t|--track STRING) [-q|--quiet] | (-m|--mplayer STRING) [--at FLOAT] | --auth)

## Available options:  
### Flags:
|                    |                                    |
|---                 |---                                 |
|-a,--artist STRING  |Artist name of the track to scrobble|
|-t,--track STRING   |Title of the track to scrobble      |
|-q,--quiet          |Whether to be quiet                 |
|-m,--mplayer STRING |Arguments of mplayer                |
|--at FLOAT          |Scrobbling point of the track (%) (default: 50)|
|--auth              |Do authentication                   |
|-h,--help           |Show this help text                 |
  

### Environment Variables:  
|                       |                                      |
|---                    |---                                   |
|$HSCROBBLER_APIKEY     |https://www.last.fm/api/account/create|
|$HSCROBBLER_SECRET     |https://www.last.fm/api/account/create|
|$HSCROBBLER_SESSIONKEY |hScrobbler --auth                     |
